﻿#include <iostream>
using namespace std;

void example(bool value,int N)
{
    for (int i = value; i <= N; i += 2)
    {
        cout << i << " ";
    }
}

int main()
{
    int N;
    cout<<"Enter limit: ";
    cin>>N;
    bool value;
    for (int i = 0; i <= N; i += 2)
    {
        cout << i << " ";
    }
    cout << endl;
    cout << "Enter 1 or 0" << endl;
    cin >> value;
    example(value,N);
    return 0;
}

